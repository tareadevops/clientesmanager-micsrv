package net.codejava;

import java.util.Locale;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.jupiter.api.Test;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import static org.assertj.core.api.Assertions.assertThat;

class ClientesTests {

	private Validator createValidator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.afterPropertiesSet();
		return localValidatorFactoryBean;
	}

	@Test
	void shouldNotValidateWhenNameEmpty() {

		LocaleContextHolder.setLocale(Locale.ENGLISH);
		Clientes cliente = new Clientes();
		//cliente.setName("");
		cliente.setLastName("Molsa");
		cliente.setCountry("El Salvador");
		cliente.setPrice(10.5f);

		Validator validator = createValidator();
		Set<ConstraintViolation<Clientes>> constraintViolations = validator.validate(cliente);

		assertThat(constraintViolations).hasSize(1);
		ConstraintViolation<Clientes> violation = constraintViolations.iterator().next();
		assertThat(violation.getPropertyPath().toString()).isEqualTo("name");
		assertThat(violation.getMessage()).isEqualTo("must not be empty");
	}

}
