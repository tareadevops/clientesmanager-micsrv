package net.codejava;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ClientesIntegrationTests {

	@Autowired
	private ClientesRepository productRepo;
	
	@Test
	void testFindAll() throws Exception {
		productRepo.findAll();
	}

	@Test
	void testByCreate() throws Exception {
		productRepo.save(new Clientes(10L, "Cuaderno", "Facela", "El Salvador", 1.85f));
	}

	@Test
	void testByDelete() throws Exception {
		productRepo.deleteById(1L);
	}

}
