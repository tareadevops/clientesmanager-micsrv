package net.codejava;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
class ClientesControllerTests {

	private static final long TEST_cliente_ID = 1L;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ClientesRepository repo;

	private Clientes cliente;

	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeEach
	void setup() {
		cliente = new Clientes();
		
		cliente.setId(TEST_cliente_ID);
		cliente.setName("Harina");
		cliente.setLastName("Molsa");
		cliente.setCountry("El Salvador");
		cliente.setPrice(10.50f);
		
		Optional<Clientes> clienteOpt = Optional.of(cliente);
			
		given(this.repo.findById(TEST_cliente_ID)).willReturn(clienteOpt);
	}
	
	@Test
	void testInitGetForm() throws Exception {
		mockMvc.perform(get("/api/clientes"))
		        .andExpect(status().isOk())
		        .andReturn();
	}
	
	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(post("/api/clientes/new")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(objectMapper.writeValueAsString(cliente))
	            )
				
	            ;
	}

	@Test
	void testInitSaveForm() throws Exception {
		mockMvc.perform(post("/api/clientes/save")
		        .contentType(MediaType.APPLICATION_JSON)
		        .content(objectMapper.writeValueAsString(cliente))
		        )
				
	            ;
	}
	
	@Test 
	void testGetEditionForm() throws Exception {
		mockMvc.perform(get("/api/clientes/{id}", TEST_cliente_ID)
		        .contentType(MediaType.APPLICATION_JSON)
		        .content(objectMapper.writeValueAsString(cliente))
		        )
				.andExpect(status().isOk())
				
		        ;
	}
	
	
	@Test
	void testProcessDelete() throws Exception {
		mockMvc.perform(delete("/api/clientes/delete/{id}", TEST_cliente_ID))
				
				;
	}

	
	public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
