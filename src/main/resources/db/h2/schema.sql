DROP TABLE IF EXISTS clientes;

CREATE TABLE clientes (
  id serial,
  name varchar(45) NOT NULL,
  last_name varchar(45) NOT NULL,
  country varchar(45) NOT NULL,
  price float NOT NULL,
  PRIMARY KEY (id)
); 