package net.codejava;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientesRepository extends JpaRepository<Clientes, Long> {

	@Query(value = "Select c.* from Clientes c where c.name like :param", nativeQuery = true)
	Collection<Clientes> findByName(@Param("param") String param);
}
