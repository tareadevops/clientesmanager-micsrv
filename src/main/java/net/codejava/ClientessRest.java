/**
 * 
 */
package net.codejava;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 * @author nelson.garcia
 *
 */
@RestController
@RequestMapping("/api/clientes")
public class ClientessRest {

	@Autowired
	private ClientesService service;
	
	@GetMapping
	public List<Clientes> listar(){
		return service.listAll();
	}
	
	@GetMapping("/{id}")
	public Clientes listarPorId(@PathVariable("id") Integer id) {
		return service.get(id);
	}
	
	@PostMapping
	public Clientes registrar(@RequestBody Clientes cliente) {
		service.save(cliente);
		return cliente;
	}
	
	@PutMapping
	public Clientes edit(@RequestBody Clientes cliente) {
		service.save(cliente);
		return cliente;
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Integer id) {
		service.delete(id);
	}
}
