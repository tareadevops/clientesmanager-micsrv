
FROM openjdk:8-jdk-alpine  
WORKDIR /workspace
COPY /target/clientes*.jar app.jar
EXPOSE 9090
ENTRYPOINT exec java -jar /workspace/app.jar
