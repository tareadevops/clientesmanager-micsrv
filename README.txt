Tipo de Aplicación : Microservicio
Nro. Descripción
1 Utilizar una aplicación que disponga hecha en Maven que genere un artefacto de
tipo FAT JAR. Se recomienda usar Spring Boot.
2 Crear un repositorio bitbucket público y alojar la aplicación.
3 Crear un pipeline(Jenkinsfile) con los siguientes stages:
Build
Testing(Junit + Jacoco)
Sonar (covertura mayor a 60%)
Despliegue en Dockerhub (Use un Dockerfile)